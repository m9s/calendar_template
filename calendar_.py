# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import uuid
import datetime
from dateutil import rrule
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Bool, Eval
from trytond.cache import Cache
from trytond.pool import Pool


_STRING_TO_FREQ = {
    'yearly': rrule.YEARLY,
    'monthly': rrule.MONTHLY,
    'weekly': rrule.WEEKLY,
    'daily': rrule.DAILY,
    'hourly': rrule.HOURLY,
    'minutely': rrule.MINUTELY,
    'secondly': rrule.SECONDLY,
    }


class Template(ModelSQL, ModelView):
    'Calendar Template'
    _name = 'calendar.template'
    _description = __doc__

    name = fields.Char('Name', required=True, translate=True, loading='lazy')
    description = fields.Text('Description', translate=True, loading='lazy')

Template()


class Calendar(ModelSQL, ModelView):
    _name = 'calendar.calendar'

    @Cache('calendar_calendar.list_event_dates')
    def get_event_ruleset(self, calendar_id):
        event_obj = Pool().get('calendar.event')
        set = rrule.rruleset()
        event_ids = event_obj.search([
                ('calendar', '=', calendar_id),
                ('parent', '=', False)
                ])
        events = event_obj.browse(event_ids)
        for event in events:
            set.rdate(event.dtstart)
            for rrule_record in event.rrules:
                set.rrule(rrule.rrule(
                        _STRING_TO_FREQ[rrule_record.freq],
                        until = rrule_record.until or event.dtend or None,
                        count = rrule_record.count or None,
                        dtstart = event.dtstart or None,
                        interval = rrule_record.interval or 1,
                        bysecond = rrule_record.bysecond or None,
                        byminute = rrule_record.byminute or None,
                        byhour = rrule_record.byhour or None,
                        byweekday = rrule_record.byday or None,
                        bymonthday = rrule_record.bymonthday or None,
                        byyearday = rrule_record.byyearday or None,
                        byweekno = rrule_record.byweekno or None,
                        bymonth = rrule_record.bymonth or None,
                        bysetpos = rrule_record.bysetpos or None,
                        wkst = rrule_record.wkst or None,
                        ))
            for rdate_record in event.rdates:
                set.rdate(rdate_record.datetime)
            for exrule_record in event.exrules:
                set.exrule(rrule.rrule(
                        _STRING_TO_FREQ[exrule_record.freq],
                        until = exrule_record.until or event.dtend or None,
                        count = exrule_record.count or None,
                        dtstart = event.dtstart or None,
                        interval = exrule_record.interval or 1,
                        bysecond = exrule_record.bysecond or None,
                        byminute = exrule_record.byminute or None,
                        byhour = exrule_record.byhour or None,
                        byweekday = exrule_record.byday or None,
                        bymonthday = exrule_record.bymonthday or None,
                        byyearday = exrule_record.byyearday or None,
                        byweekno = exrule_record.byweekno or None,
                        bymonth = exrule_record.bymonth or None,
                        bysetpos = exrule_record.bysetpos or None,
                        wkst = exrule_record.wkst or None,
                        ))
            for exdate_record in event.exdates:
                set.exdate(exdate_record.datetime)
            for occurence_record in event.occurences:
                set.rdate(occurence_record.dtstart)
        return set

    def check_event_match(self, date_time, calendar_id):
        date_time = datetime.datetime(date_time.year, date_time.month,
                date_time.day, 0, 0)
        event_ruleset = self.get_event_ruleset(calendar_id)
        if event_ruleset.between(date_time, date_time, inc=True):
            return True
        return False

Calendar()


class EventTemplate(ModelSQL, ModelView):
    'Event Template'
    _name = 'calendar.template.event'
    _description = __doc__

    calendar_template = fields.Many2One('calendar.template',
            'Calendar Template', required=True, ondelete='CASCADE')
    summary = fields.Char('Summary', translate=True, loading='lazy')
    sequence = fields.Integer('Sequence')
    description = fields.Text('Description')
    all_day = fields.Boolean('All Day')
    dtstart = fields.DateTime('Start Date', required=True, select=1)
    dtend = fields.DateTime('End Date', select=1)
    timezone = fields.Selection('timezones', 'Timezone')
    classification = fields.Selection([
            ('public', 'Public'),
            ('private', 'Private'),
            ('confidential', 'Confidential'),
            ], 'Classification', required=True)
    transp = fields.Selection([
            ('opaque', 'Opaque'),
            ('transparent', 'Transparent'),
            ], 'Time Transparency', required=True)
    rdates = fields.One2Many('calendar.template.event.rdate', 'template',
            'Recurrence Dates',
            states={
                'invisible': Bool(Eval('parent')),
            }, depends=['parent'])
    rrules = fields.One2Many('calendar.template.event.rrule', 'template',
            'Recurrence Rules',
            states={
                'invisible': Bool(Eval('parent')),
            }, depends=['parent'])
    exdates = fields.One2Many('calendar.template.event.exdate', 'template',
            'Exception Dates',
            states={
                'invisible': Bool(Eval('parent')),
            }, depends=['parent'])
    exrules = fields.One2Many('calendar.template.event.exrule', 'template',
            'Exception Rules',
            states={
                'invisible': Bool(Eval('parent')),
            }, depends=['parent'])
    occurences = fields.One2Many('calendar.template.event', 'parent',
            'Occurences',
            domain=[
                ('calendar', '=', Eval('calendar')),
            ],
            states={
                'invisible': Bool(Eval('parent')),
            }, depends=['calendar', 'parent'])
    parent = fields.Many2One('calendar.template.event', 'Parent',
            domain=[
                ('parent', '=', False),
                ('calendar', '=', Eval('calendar')),
            ],
            ondelete='CASCADE', depends=['calendar'])

    def create_event(self, template, calendar, dtstart=False, dtend=False):
        event_obj = Pool().get('calendar.event')
        vals = self._get_event_values_from_template(template, calendar,
            dtstart=dtstart, dtend=dtend)
        vals['calendar'] = calendar.id
        new_id = event_obj.create(vals)
        if template.occurences:
            for occurrence in template.occurences:
                vals = self._get_event_values_from_template(self, template,
                        calendar)
                vals['calendar'] = calendar.id
                vals['parent'] = new_id
                occurrence_id = event_obj.create(vals)
        return new_id

    def _get_event_values_from_template(self, template, calendar,
            dtstart=False, dtend=False):
        pool = Pool()
        rdate_template_obj = pool.get('calendar.template.event.rdate')
        exdate_template_obj = pool.get('calendar.template.event.exdate')
        rrule_template_obj = pool.get('calendar.template.event.rrule')
        exrule_template_obj = pool.get('calendar.template.event.exrule')
        new_uuid = str(uuid.uuid4())
        res = {
            'uuid': new_uuid,
            'calendar': calendar.id,
            'summary': template.summary,
            'event_template': template.id,
            'sequence': template.sequence,
            'description': template.description,
            'all_day': template.all_day,
            'dtstart': template.dtstart,
            'dtend': template.dtend,
            'timezone': template.timezone,
            'classification': template.classification,
            'transp': template.transp,
            }

        res['rdates'] = []
        for record in template.rdates:
            rdate_vals =  rdate_template_obj.get_values_from_template(record)
            res['rdates'] += [('create', rdate_vals)]
        res['exdates'] = []
        for record in template.exdates:
            exdate_vals = exdate_template_obj.get_values_from_template(record)
            res['exdates'] += [('create', exdate_vals)]

        easter_dtstart = False
        res['rrules'] = []
        for record in template.rrules:
            if record.byeaster:
                date_vals = rrule_template_obj._get_dates_from_easter_rule(
                    record, dtstart=dtstart, dtend=dtend)
                for value in date_vals:
                    if dtstart:
                        if not easter_dtstart:
                            easter_dtstart = value
                        elif value < easter_dtstart:
                            easter_dtstart = value
                    rdate_vals = {
                        'date': True,
                        'datetime': value,
                        }
                    res['rdates'] += [('create', rdate_vals)]
            else:
                rrule_vals = rrule_template_obj.get_values_from_template(record)
                if rrule_vals.get('until') and dtend:
                    if rrule_vals['until'] > dtend:
                        rrule_vals['until'] = dtend
                elif dtend:
                    rrule_vals['until'] = dtend
                res['rrules'] += [('create', rrule_vals)]
        res['exrules'] = []
        for record in template.exrules:
            if record.byeaster:
                date_vals = exrule_template_obj._get_dates_from_easter_rule(
                    record, dtstart=dtstart, dtend=dtend)
                for value in date_vals:
                    ex_date_vals = {
                        'date': True,
                        'datetime': value,
                        }
                    res['exdates'] += [('create', ex_date_vals)]
            else:
                exrule_vals = exrule_template_obj.get_values_from_template(
                        record)
                if exrule_vals.get('until') and dtend:
                    if exrule_vals['until'] > dtend:
                        exrule_vals['until'] = dtend
                elif dtend:
                    exrule_vals['until'] = dtend
                res['exrules'] += [('create', exrule_vals)]

        if dtstart:
            if easter_dtstart:
                dtstart = easter_dtstart
            elif template.dtstart:
                dtstart = datetime.datetime(dtstart.year,
                    template.dtstart.month, template.dtstart.day,
                    template.dtstart.hour, template.dtstart.minute,
                    template.dtstart.second)
            res['dtstart'] = dtstart

        return res

EventTemplate()


class Event(ModelSQL, ModelView):
    _name = 'calendar.event'

    event_template = fields.Many2One('calendar.template.event',
            'Event Template')

    def update_event(self, event, dtstart=False, dtend=False):
        template_obj = Pool().get('calendar.template.event')
        if event.event_template:
            vals = template_obj._get_event_values_from_template(
                    event.event_template, event.calendar,
                    dtstart=dtstart, dtend=dtend)
            vals['exrules'] += [('delete', [x.id for x in event.exrules])]
            vals['rrules'] += [('delete', [x.id for x in event.rrules])]
            vals['exdates'] += [('delete', [x.id for x in event.exdates])]
            vals['rdates'] += [('delete', [x.id for x in event.rdates])]
            vals.setdefault('occurences', [])
            vals['occurences'] += [('delete', [x.id for x in event.occurences])]
            self.write(event.id, vals)
        return True

Event()


class EventRDateTemplate(ModelSQL, ModelView):
    'Event Recurrence Date Template'
    _description = __doc__
    _name = 'calendar.template.event.rdate'
    _inherits = {'calendar.date': 'calendar_date'}
    _rec_name = 'datetime'

    calendar_date = fields.Many2One('calendar.date', 'Calendar Date',
            required=True, ondelete='CASCADE', select=1)
    template = fields.Many2One('calendar.template.event',
            'Event Template', ondelete='CASCADE', select=1, required=True)

    def get_values_from_template(self, template):
        res = {
            'date': template.date,
            'datetime': template.datetime
            }
        return res

EventRDateTemplate()


class EventExDateTemplate(EventRDateTemplate):
    'Event Exception Date Template'
    _description = __doc__
    _name = 'calendar.template.event.exdate'

EventExDateTemplate()


class EventRRuleTemplate(ModelSQL, ModelView):
    'Event Recurrence Rule Template'
    _description = __doc__
    _name = 'calendar.template.event.rrule'
    _inherits = {'calendar.rrule': 'calendar_rrule'}
    _rec_name = 'freq'

    calendar_rrule = fields.Many2One('calendar.rrule', 'Calendar RRule',
            required=True, ondelete='CASCADE', select=1)
    template = fields.Many2One('calendar.template.event',
            'Event Template', ondelete='CASCADE', select=1, required=True)
    byeaster = fields.Char('By Easter')

    def get_values_from_template(self, template):
        res = {
            'freq': template.freq,
            'until_date': template.until_date,
            'until': template.until,
            'count': template.count,
            'interval': template.interval,
            'bysecond': template.bysecond,
            'byminute': template.byminute,
            'byhour': template.byhour,
            'byday': template.byday,
            'bymonthday': template.bymonthday,
            'byyearday': template.byyearday,
            'byweekno': template.byweekno,
            'bymonth': template.bymonth,
            'bysetpos': template.bysetpos,
            'wkst': template.wkst,
            }
        return res

    def get_dateutil_values_from_template(self, template):
        res = {
            'until': template.until,
            'count': template.count or None,
            'interval': template.interval or 1,
            'bysecond': template.bysecond,
            'byminute': template.byminute,
            'byhour': template.byhour,
            'byweekday': template.byday,
            'bymonthday': template.bymonthday,
            'byyearday': template.byyearday,
            'byweekno': template.byweekno,
            'bymonth': template.bymonth,
            'bysetpos': template.bysetpos,
            'wkst': template.wkst,
            'byeaster': int(template.byeaster)
            }
        return res

    def _get_dates_from_easter_rule(self, rule, dtstart=False,
            dtend=False):
        vals = self.get_dateutil_values_from_template(rule)
        # dateutil uses time from now() when no dtstart is given
        vals['dtstart'] = datetime.datetime(1900,1,1,0,0,0)
        easter_rule = rrule.rrule(_STRING_TO_FREQ[rule.freq], **vals)
        if not dtstart:
            dtstart = datetime.datetime(1900,1,1,0,0,0)
        if not dtend:
            dtend = datetime.datetime(2100,12,31,23,59,59)
        return easter_rule.between(dtstart, dtend, inc=True)

EventRRuleTemplate()


class EventExRuleTemplate(EventRRuleTemplate):
    'Event Exception Rule Template'
    _description = __doc__
    _name = 'calendar.template.event.exrule'

EventExRuleTemplate()


class UpdateCalendarInit(ModelView):
    'Update Calendar from Template Init'
    _name = 'calendar.template.update_calendar.init'
    _description = __doc__

    templates = fields.Many2Many('calendar.template', None, None, 'Templates',
        required=True)
    calendar = fields.Many2One('calendar.calendar', 'Calendar', required=True)
    start_date = fields.DateTime('Start Date', required=True,
        on_change=['start_date', 'end_date'])
    end_date = fields.DateTime('End Date', required=True,
        on_change=['start_date', 'end_date'])

    def on_change_start_date(self, vals):
        res = {}
        if vals.get('start_date') and vals.get('end_date'):
            if vals['end_date'] < vals['start_date']:
                res['end_date'] = False
        return res

    def on_change_end_date(self, vals):
        res = {}
        if vals.get('start_date') and vals.get('end_date'):
            if vals['end_date'] < vals['start_date']:
                res['start_date'] = False
        return res

UpdateCalendarInit()


class UpdateCalendar(Wizard):
    'Update Calendar from Template'
    _name = 'calendar.template.update_calendar'

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'calendar.template.update_calendar.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('update', 'Ok', 'tryton-ok', True),
                ],
            },
        },
        'update': {
            'result': {
                'type': 'action',
                'action': '_action_update_from_template',
                'state': 'end',
            },
        },
    }

    def _action_update_from_template(self, data):
        pool = Pool()
        calendar_obj = pool.get('calendar.calendar')
        event_obj = pool.get('calendar.event')
        event_template_obj = pool.get('calendar.template.event')

        calendar = calendar_obj.browse(data['form']['calendar'])
        start_date = data['form']['start_date']
        end_date = data['form']['end_date']
        args = [
                ('calendar_template', 'in', data['form']['templates'][0][1]),
                ('parent', '=', False)
            ]
        event_template_ids = event_template_obj.search(args)

        events_to_update_ids = event_obj.search([
                ('event_template', 'in', event_template_ids),
                ('calendar', '=', calendar.id),
                ('parent', '=', False)
            ])
        template_ids_to_process = event_template_ids
        events_to_update = event_obj.browse(events_to_update_ids)
        for event in events_to_update:
            event_obj.update_event(event, dtstart=start_date,
                dtend=end_date)
            template_ids_to_process.remove(event.event_template.id)

        event_templates = event_template_obj.browse(template_ids_to_process)
        for event_template in event_templates:
            event_template_obj.create_event(event_template, calendar,
                dtstart=start_date, dtend=end_date)
        return {}

UpdateCalendar()

#!/usr/bin/env python
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import sys, os
DIR = os.path.abspath(os.path.normpath(os.path.join(__file__,
    '..', '..', '..', '..', '..', 'trytond')))
if os.path.isdir(DIR):
    sys.path.insert(0, os.path.dirname(DIR))

import unittest
import datetime
import trytond.tests.test_tryton
from trytond.tests.test_tryton import POOL, DB_NAME, USER, CONTEXT, test_view,\
    test_depends
from trytond.transaction import Transaction


class CalendarTemplateTestCase(unittest.TestCase):
    '''
    Test CalendarTemplate module.
    '''

    def setUp(self):
        trytond.tests.test_tryton.install_module('calendar_template')
        self.calendar = POOL.get('calendar.calendar')
        self.event = POOL.get('calendar.event')

    def test0005views(self):
        '''
        Test views.
        '''
        test_view('calendar_template')

    def test0006depends(self):
        '''
        Test depends.
        '''
        test_depends()

    def test0010rruleset(self):
        with Transaction().start(DB_NAME, USER, CONTEXT) as transaction:
            calendar1_id = self.calendar.create({
                'name': 'Holiday Calendar',
                'read_users': [('add', [USER])],
                'write_users': [('add', [USER])]
                })
            self.assert_(calendar1_id)
            event1_id = self.event.create({
                'calendar': calendar1_id,
                'summary': 'Testtermin',
                'dtstart': datetime.datetime(2011,05,24,0,0),
                'rrules': [('create', {
                    'freq': 'yearly',
                    'until': datetime.datetime(2015,05,24,0,0)
                    })]
                })
            self.assert_(event1_id)
            transaction.cursor.commit()

        result = [
            datetime.datetime(2011, 5, 24, 0, 0),
            datetime.datetime(2012, 5, 24, 0, 0),
            datetime.datetime(2013, 5, 24, 0, 0),
            datetime.datetime(2014, 5, 24, 0, 0),
            datetime.datetime(2015, 5, 24, 0, 0)]
        with Transaction().start(DB_NAME, USER, CONTEXT) as transaction:
            ruleset = self.calendar.get_event_ruleset(calendar1_id)
            self.assert_(result == list(ruleset))

def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        CalendarTemplateTestCase))
    return suite

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(suite())

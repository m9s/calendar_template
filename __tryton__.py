# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Calendar Template',
    'name_de_DE': 'Kalender Vorlage',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Provides templates for calendars
    ''',
    'description_de_DE': '''
    - Stellt Kalendervorlagen zur Verfügung.
    ''',
    'depends': [
        'calendar'
    ],
    'xml': [
        'calendar.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
